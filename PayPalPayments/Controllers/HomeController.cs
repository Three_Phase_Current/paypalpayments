﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using PayPalPayments.Services;

namespace PayPalPayments.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult CreatePayment()
        {
            var payment = PayPalPaymentService.CreatePayment("https://localhost:44385", "sale");

            return Redirect(payment.GetApprovalUrl());
        }

        public ActionResult PaymentCancelled()
        {
            return HttpNotFound("Ops... something was wrong with your payment");
        }

        public ActionResult PaymentSuccessful(string paymentId, string token, string PayerID)
        {
            // Execute Payment
            var payment = PayPalPaymentService.ExecutePayment(paymentId, PayerID);

            return View();
        }
    }
}